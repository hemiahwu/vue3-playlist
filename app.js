const { ref, reactive } = VueCompositionAPI;

new Vue({
  el: "#vue-app", // element
  setup() {
    const obj = reactive({ name: "米修在线", wechat: "27732357" });
    return {
      obj,
    };
  },
});
